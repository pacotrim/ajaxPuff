$.ajax({
    url: "https://icuff17-api.herokuapp.com/teachers/"
}).done(function(data){
    for(var i = 0; i<data.length ; i++){
        var div=$('<div></div>').attr('class', 'professor');
        div.append($('<h2></h2').text(data[i].name));
        div.append($('<p></p>').text(data[i].subject));
        div.append($('<img>').attr('src',data[i].photo));
        $('body').append(div);
    }
})